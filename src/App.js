import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>DevOps - Continuos Deployment.</p>
        <a className="App-link" href="https://www.unimagdalena.edu.co/" target="_blank" rel="noopener noreferrer">
          Universidad del Magdalena
        </a>
      </header>
    </div>
  );
}

export default App;
